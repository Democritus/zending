﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePattern {

	public bool movingOut;
	public float progress; // 0 - 1
	public Vector3 offset;
	public float duration;
	public Easing.EaseType easeType = Easing.EaseType.SinusoidalInOut;

	public Vector3 GetMoveOffset(float deltaTime) {
		if (movingOut)
			progress += deltaTime / duration;
		else 
			progress -= deltaTime / duration;

		if (progress >= 1f || progress <= 0f)
			movingOut = !movingOut;

		return offset * Mathf.Clamp01(Easing.Ease(easeType, progress));  
	}
}

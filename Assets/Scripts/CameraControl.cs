﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraControl : MonoBehaviour {

    private float zoomScaleIn = 5f; 
    private float zoomScaleOut = 20f; 

    public float zoomSpeed = 0.1f;
    public AnimationCurve zoomCurve; // evaluated to provide some easing across the whole zoom range

    // ranges from 0 to 1, with 0 zoomed in all the way, 1 zoomed out all the way
    // Change these to adjust starting zoom
    private float currentZoom = 0.5f; 
    private float targetZoom = 0.5f;
    private const float zoomTolerance = 0.00001f; // snap when this close to target

	public float dragSpeed = 0.1F;

	private Camera cam;

    protected void Start() {
        Init();
    }

    private void Init() {
        enabled = true;
		cam = GetComponent<Camera>();
        SetScale(currentZoom);
    }

    void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f && targetZoom > 0f) {
            targetZoom -= 0.1f;
            targetZoom = Mathf.Clamp01(targetZoom);
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f && targetZoom < 1f) {
            targetZoom += 0.1f;
            targetZoom = Mathf.Clamp01(targetZoom);
        }

        if (targetZoom != currentZoom) {
            if (Mathf.Approximately(targetZoom, currentZoom))
                currentZoom = targetZoom;
            else
                currentZoom += (targetZoom - currentZoom) * Time.deltaTime * zoomSpeed;

            SetScale(currentZoom);
        }

		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			transform.Translate(-touchDeltaPosition.x * dragSpeed, -touchDeltaPosition.y * dragSpeed, 0);
		}
    }

    // Here we get the currentZoom between 0 (zoomed in) and 1 (zoomed out)
    // We use that to evaluate the curve to get another value between 0 and 1. This distorts the zoom so that zooming out is slower
    // Then we use that value to get a scale factor between our min and max zoomScales and put that in the canvas
    void SetScale(float zoom) {
		cam.orthographicSize = Mathf.Lerp(zoomScaleIn, zoomScaleOut, zoomCurve.Evaluate(zoom));
    }

}

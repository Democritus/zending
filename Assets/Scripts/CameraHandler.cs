﻿using UnityEngine;
using System.Collections;

public class CameraHandler : MonoBehaviour {

	private static readonly float PanSpeed = 20f;
	private static readonly float ZoomSpeedTouch = 0.1f;
	private static readonly float ZoomSpeedMouse = 2f;

	private static readonly float[] BoundsX = new float[]{ 0f, 40f };
	private static readonly float[] BoundsY = new float[]{ 0f, 40f };
	private static readonly float[] ZoomBounds = new float[]{ 5f, 20f };

	private Camera cam;

	private Vector3 lastPanPosition;
	private int panFingerId;
	// Touch mode only
	private bool isPanning;

	private bool wasZoomingLastFrame;
	// Touch mode only
	private float lastZoomTouchDistance;
	// Touch mode only

	void Awake() {
		cam = GetComponent<Camera>();
	}

	void Update() {
		if (GameController.isGameRunning == false)
			return;
		
		if (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer) {
			HandleTouch();
		}
		else {
			HandleMouse();
		}
	}

	void HandleTouch() {
		switch (Input.touchCount) {

			case 1: // Panning
				wasZoomingLastFrame = false;

				// If the touch began, capture its position and its finger ID.
				// Otherwise, if the finger ID of the touch doesn't match, skip it.
				Touch touch = Input.GetTouch(0);
				if (touch.phase == TouchPhase.Began) {
					lastPanPosition = touch.position;
					panFingerId = touch.fingerId;
				}
				else if (touch.fingerId == panFingerId && touch.phase == TouchPhase.Moved) {
					PanCamera(touch.position);
				}
				break;

			case 2: // Zooming
				Vector2[] newPositions = new Vector2[]{ Input.GetTouch(0).position, Input.GetTouch(1).position };
				if (!wasZoomingLastFrame) {
					lastZoomTouchDistance = Vector2.Distance(newPositions[0], newPositions[1]);
					wasZoomingLastFrame = true;
				}
				else {
					// Zoom based on the distance between the new positions compared to the 
					// distance between the previous positions.
					float newDistance = Vector2.Distance(newPositions[0], newPositions[1]);
					float offset = newDistance - lastZoomTouchDistance;

					ZoomCamera(offset, ZoomSpeedTouch);

					lastZoomTouchDistance = newDistance;
				}
				break;

			default: 
				wasZoomingLastFrame = false;
				break;
		}
	}

	void HandleMouse() {
		// On mouse down, capture it's position.
		// Otherwise, if the mouse is still down, pan the camera.
		if (Input.GetMouseButtonDown(0)) {
			lastPanPosition = Input.mousePosition;
			isPanning = true;
		}
		else if (isPanning && Input.GetMouseButton(0)) {
			PanCamera(Input.mousePosition);
		}
		else if (Input.GetMouseButtonUp(0)) {
			isPanning = false;
		}

		// Check for scrolling to zoom the camera
		float scroll = Input.GetAxis("Mouse ScrollWheel");
		ZoomCamera(scroll, ZoomSpeedMouse);
	}

	void PanCamera(Vector3 newPanPosition) {
		// Determine how much to move the camera
		Vector3 offset = cam.ScreenToViewportPoint(lastPanPosition - newPanPosition);
		Vector3 move = new Vector3(offset.x * PanSpeed, offset.y * PanSpeed);
		move += transform.position;
		move.x = Mathf.Clamp(move.x, BoundsX[0], BoundsX[1]);
		move.y = Mathf.Clamp(move.y, BoundsY[0], BoundsY[1]);
		transform.position = move;

		// Cache the position
		lastPanPosition = newPanPosition;
	}

	void ZoomCamera(float offset, float speed) {
		if (offset == 0) {
			return;
		}

		cam.orthographicSize = Mathf.Clamp(cam.orthographicSize - (offset * speed), ZoomBounds[0], ZoomBounds[1]);
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ThemeBase : ScriptableObject {

	[Header("Audio")]
	public int audioSet = 0;
	public int maxPulseGeneration = 9;

	abstract public Color GetBackgroundColor(); 
	abstract public Sprite GetBackgroundSprite(); 

	abstract public Sprite GetDotSprite(); 
	abstract public Color GetDotColorStrange(); 
	abstract public Color GetDotColorConverted(); 

	abstract public Sprite GetPulseSprite(); 
	abstract public Color GetPulseColor(); 
	abstract public ParticleSystem GetPulseParticle(); 

	abstract public Color GetProgressColor();

	abstract public Color GetShotColorA();
	abstract public Color GetShotColorB();
}

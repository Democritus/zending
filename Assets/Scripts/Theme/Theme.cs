﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class Theme : ThemeBase {
	 
	[Header("Background")]
	public Sprite bgSprite;
	public Color bgColor;
	public Color progressColor = new Color(0f, 0f, 0f, 0.4f);

	[Header("Dots")]
	public Sprite dotSprite;
	public Color dotStrange = Color.white;
	public Color dotConverted = Color.red;

	[Header("Pulse")]
	public Sprite pulseSprite;
	public Color pulseColor = Color.white;
	public ParticleSystem pulseParticle;

	public override Sprite GetBackgroundSprite() {
		return bgSprite;
	}

	public override Color GetBackgroundColor() {
		return bgColor;
	}

	public override Sprite GetDotSprite() {
		return dotSprite;
	}

	public override Color GetDotColorStrange() {
		return dotStrange;
	}

	public override Color GetDotColorConverted() {
		return dotConverted;
	}

	public override Sprite GetPulseSprite() {
		return pulseSprite;
	}

	public override Color GetPulseColor() {
		return pulseColor;
	}

	public override ParticleSystem GetPulseParticle() {
		return pulseParticle;
	}

	public override Color GetProgressColor() {
		return progressColor;
	}

	public override Color GetShotColorA() {
		return dotConverted;
	}

	public override Color GetShotColorB() {
		return dotConverted;
	}


}

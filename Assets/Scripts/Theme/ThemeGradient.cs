﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class ThemeGradient : ThemeBase {
	 
	[Header("Background")]
	public Sprite bgSprite;
	public Gradient bgColor;
	public Color progressColor = new Color(0f, 0f, 0f, 0.4f);

	[Header("Dots")]
	public Sprite dotSprite;
	public Gradient dotStrange;
	public Gradient dotConverted;

	[Header("Pulse")]
	public Sprite pulseSprite;
	public Gradient pulseColor;
	public ParticleSystem pulseParticle;

	public override Sprite GetBackgroundSprite() {
		return bgSprite;
	}

	public override Color GetBackgroundColor() {
		return bgColor.Evaluate(Random.value);
	}

	public override Sprite GetDotSprite() {
		return dotSprite;
	}

	public override Color GetDotColorStrange() {
		return dotStrange.Evaluate(Random.value);
	}

	public override Color GetDotColorConverted() {
		return dotConverted.Evaluate(Random.value);
	}

	public override Sprite GetPulseSprite() {
		return pulseSprite;
	}

	public override Color GetPulseColor() {
		return pulseColor.Evaluate(Random.value);
	}

	public override ParticleSystem GetPulseParticle() {
		return pulseParticle;
	}

	public override Color GetProgressColor() {
		return progressColor;
	}

	public override Color GetShotColorA() {
		return dotConverted.Evaluate(0f);
	}

	public override Color GetShotColorB() {
		return dotConverted.Evaluate(1f);
	}

}

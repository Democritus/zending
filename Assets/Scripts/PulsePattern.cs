﻿
public struct PulsePattern {
	public float maxRadius;
	public Easing.EaseType attackEase;
	public float attackTime;
	public float sustainTime;
	public Easing.EaseType decayEase;
	public float decayTime;
}
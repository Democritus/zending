﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour {

	public int numDots = 10;
	public QuadrantSpawner quadrantSpawner;
	public SpriteRenderer background;

	public Dot[] SpawnDots() {
		Vector3 texScale = new Vector3(background.sprite.rect.width / background.sprite.pixelsPerUnit,
			                   background.sprite.rect.height / background.sprite.pixelsPerUnit);
		Vector3 worldSize = new Vector3(quadrantSpawner.numColumns * quadrantSpawner.quadrantWidth,
			quadrantSpawner.numRows * quadrantSpawner.quadrantHeight);

		background.transform.localScale = new Vector3(worldSize.x / texScale.x, worldSize.y / texScale.y, 1f);

		Dot[] dots = new Dot[numDots];

		for (int i = 0; i < numDots; i++) {
			dots[i] = quadrantSpawner.SpawnObject().GetComponent<Dot>();
			dots[i].Init(GetRandomPattern());
		}

		return dots;
	}

	MovePattern GetRandomPattern() {
		if (Random.value < 0.7f)
			return null;

		float radius = 3f + Random.value * 2f;
		float angle = Random.value * Mathf.PI * 2f;

		var pattern = new MovePattern();
		pattern.duration = 4f + Random.value * 4f;
		pattern.movingOut = Random.value >= 0.5f;
		pattern.progress = Random.value;
		pattern.offset = new Vector3(Mathf.Cos(angle) * radius, Mathf.Sin(angle) * radius);

		return pattern;
	}

	public void ClearDots(Dot[] dots) {
		quadrantSpawner.Reset();

		if (dots == null)
			return;
		
		for (int i = 0; i < dots.Length; i++) {
			Destroy(dots[i].gameObject);
		}
	}
}

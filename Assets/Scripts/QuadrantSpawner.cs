﻿using UnityEngine;
using System.Collections;

public class QuadrantSpawner : MonoBehaviour {

	public Vector3 offset = Vector3.zero;
	private Vector3 lowerLeftCorner ;
	public float quadrantWidth  = 10f;
	public float quadrantHeight = 5f;
	public int numColumns = 5;
	public int numRows = 5;
	public float quadrantPaddingHorizontal = 0f;
	public float quadrantPaddingVertical = 0f;
	private bool[,] isQuadrantFilled;	// Column, then Row

	public Transform spawningObject;
	// public int numObjects = 10;

	// Use this for initialization
	void Awake () {
		Reset();
	}

	public void Reset() {		
		UpdateLowerLeftCorner();
		isQuadrantFilled = new bool[numColumns, numRows];
	}

	// SPAWN OBJECTS
	public Transform SpawnObject () {
		if (spawningObject) {
			UpdateLowerLeftCorner();
			return SpawnObject (spawningObject);
		}
		else {
			Debug.LogWarning("Trying to use QuadrantSpawner to spawn without default Object!");
			return null;
		}
	}

	public Transform SpawnObject (Transform o, int minCol = 0, int maxCol = -1, int minRow = 0, int maxRow = -1) {

		UpdateLowerLeftCorner();

		// in case we have the default values for the max, set them here.
		if (maxCol < 0) {
			maxCol = numColumns;
		}
		if (maxRow < 0) {
			maxRow = numRows;
		}

		// get a random Quadrant
		int targetCol;
		int targetRow;
		targetCol = GetRandomCol(minCol, maxCol);
		targetRow = GetRandomRow(minRow, maxRow);

		int i = 0;
		while (isQuadrantFilled[targetCol, targetRow])
		{
			// get a random Quadrant
			targetCol = GetRandomCol(minCol, maxCol);
			targetRow = GetRandomRow(minRow, maxRow);

			i++;
			if (i > 20) {
				Debug.LogWarning("QuadrantSpawner trying to look for empty Quadrant 20 times, nothing found. Possibly all filled?");
				break;
			}
		}

		Transform newObject;
		newObject = Instantiate(o, GetRandomPointInQuadrant(targetCol,targetRow), Quaternion.identity) as Transform;
		isQuadrantFilled[targetCol,targetRow] = true;

		return newObject;
	}


	private void UpdateLowerLeftCorner() {
		if (lowerLeftCorner != (transform.position + offset) ) {
			lowerLeftCorner = (transform.position + offset);
		}
	}

	// POSITIONING STUFF
	private int GetRandomCol () {
		return (int) Random.Range(0, numColumns);
	}
	private int GetRandomCol (int minCol, int maxCol) {
		return (int) Random.Range(Mathf.Min (minCol, numColumns), Mathf.Min (maxCol, numColumns) );
	}

	private int GetRandomRow () {
		return (int) Random.Range(0, numRows);
	}
	private int GetRandomRow (int minRow, int maxRow) {
		return (int) Random.Range(Mathf.Min (minRow, numRows), Mathf.Min (maxRow, numRows) );
	}

	// 0 is leftmost edge on the grid
	private float getQuadrantColumnEdge(int col) {
		return lowerLeftCorner.x + quadrantWidth * col;
	}
	// 0 is lowest edge on the grid
	private float getQuadrantRowEdge(int row) {
		return lowerLeftCorner.y + quadrantHeight * row;
	}

	private Vector3 GetRandomPointInQuadrant(int col, int row) {

		float x;
		float y;

		x = Random.Range(getQuadrantColumnEdge(col  )+quadrantPaddingHorizontal,
			getQuadrantColumnEdge(col+1)-quadrantPaddingHorizontal);
		y = Random.Range(getQuadrantRowEdge(row  )+quadrantPaddingVertical,
			getQuadrantRowEdge(row+1)-quadrantPaddingVertical);

		// Debug.Log ("RandomPoint in "+col+"/"+row+": "+x+", "+y);

		return new Vector3(x,y,0f);
	}

	// EDITOR GIZMOS
	private void OnDrawGizmosSelected () {

		lowerLeftCorner = transform.position + offset;

		Gizmos.color = Color.yellow;

		for (int i = 0; i <= numColumns; i++) {
			Vector3 startPoint = new Vector3(getQuadrantColumnEdge(i), lowerLeftCorner.y, 0f);
			Gizmos.DrawLine (startPoint, startPoint + new Vector3(0f, quadrantHeight, 0f) * numRows);
		}
		for (int j = 0; j <= numRows; j++) {
			Vector3 startPoint = new Vector3(lowerLeftCorner.x, getQuadrantRowEdge(j), 0f);
			Gizmos.DrawLine (startPoint, startPoint + new Vector3(quadrantWidth, 0f, 0f) * numColumns);
		}
	}
}
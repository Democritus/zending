﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudControl : MonoBehaviour {

	public SuperBlur.SuperBlur blur;
	public SpriteRenderer progress;

	public SpriteRenderer[] shotEmpty;
	public SpriteRenderer[] shotFilled;

	float lastProgress;
	float currentProgress;
	float targetProgress;

	public void SetBlur(bool enabled) {
		blur.enabled = enabled;
	}

	public void UpdateProgress(float percentage) {
		lastProgress = currentProgress;
		targetProgress = percentage;
		StopAllCoroutines();
		StartCoroutine(DoProgressAnim(0.5f));
	}

	IEnumerator DoProgressAnim(float duration) {
		float time = 0f;

		while (time < duration) {
			time += Time.deltaTime;
			currentProgress = Mathf.Lerp(lastProgress, targetProgress, Easing.Ease(Easing.EaseType.ExponentialOut, time / duration));
			progress.material.SetFloat("_Fill", currentProgress);
			yield return null;
		}

		progress.material.SetFloat("_Fill", targetProgress);
	}

	public void SetProgressColor(Color col) {
		progress.color = col;
	}

	public void ShowShots(bool show) {
		for (int i = 0; i < shotEmpty.Length; i++) 
			shotEmpty[i].gameObject.SetActive(show);
	}

	public void SetShotColors(Color colA, Color colB) {
		Color col;

		for (int i = 0; i < shotFilled.Length; i++) {
			col = Color.Lerp(colA, colB, i / (float)shotFilled.Length);
			col.a = 0.5f;
			shotFilled[i].color = col;
			shotEmpty[i].color = col;
		}
	}

	public void UpdateShots(int shotsRemaining) {
		for (int i = 0; i < shotFilled.Length; i++) {
			shotFilled[i].gameObject.SetActive(i + shotsRemaining >= shotFilled.Length);
		}
	}
}

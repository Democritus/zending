﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HoldButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	const float holdDuration = 1f;

	bool isHolding;
	float heldTimer;
	bool hasFired;

	public Image holdImg;
	public UnityEngine.Events.UnityEvent ActivationEvent;

	AudioSource loopSound;

	public void Reset() {
		hasFired = false;
		isHolding = false;
		heldTimer = 0f;
		enabled = true;
		SetHoldProgress(0f);
	}

	public void OnPointerDown(PointerEventData eventData) {
		isHolding = true;
		SoundManager.PlaySfx("snd_bttn_klickon_1time");
		loopSound = SoundManager.PlaySfx("snd_bttn_pressed_loop");
	}

	public void OnPointerUp(PointerEventData eventData) {
		isHolding = false;

		if (!hasFired)
			SoundManager.PlaySfx("snd_bttn_klickoff_1time");

		if (loopSound) {
			loopSound.Stop();
			loopSound = null;
		}
	}

	void Update() {
		if (isHolding)
			heldTimer += Time.deltaTime;
		else if (heldTimer > 0f)
			heldTimer -= Time.deltaTime * 0.5f;
		else if (heldTimer < 0f)
			heldTimer = 0f;

		if (heldTimer >= holdDuration)
			FireButton();
		else
			SetHoldProgress(heldTimer / holdDuration);
	}

	void SetHoldProgress(float percentage) {
		holdImg.fillAmount = percentage;

		if (loopSound != null)
			loopSound.volume = percentage;
	}

	void FireButton() {
		if (hasFired)
			return;

		hasFired = true;
		holdImg.fillAmount = 1f;
		enabled = false; // disable update

		SoundManager.PlaySfx("snd_bttn_finalrelease_1time");
		loopSound.Stop();
		loopSound = null;

		ActivationEvent.Invoke();
	}
}

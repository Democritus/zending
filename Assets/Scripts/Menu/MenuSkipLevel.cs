﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSkipLevel : MonoBehaviour {

	public HoldButton btnLast;
	public HoldButton btnNext;

	public void ShowNext(bool show) {
		btnNext.gameObject.SetActive(show);
		btnNext.Reset();

		if (show) {
			btnNext.transform.localScale = Vector3.zero;
			StartCoroutine(DoShowAnim(btnNext.transform));
		}
	}

	public void ShowRedo(bool show) {
		btnLast.gameObject.SetActive(show);
		btnLast.Reset();

		if (show) {
			btnLast.transform.localScale = Vector3.zero;
			StartCoroutine(DoShowAnim(btnLast.transform));
		}
	}


	public void TriggerEnd() {
		GameController.SendMessage(GameMessage.NextLevel);
	}

	public void TriggerRestart() {
		GameController.SendMessage(GameMessage.RestartLevel);
	}

	IEnumerator DoShowAnim(Transform animObj) {
		const float duration = 2f;
		float time = 0f;

		while (time < duration) {
			time += Time.deltaTime;
			animObj.localScale = Vector3.one * Easing.Ease(Easing.EaseType.BackOut, time / duration);
			yield return null;
		}

		animObj.localScale = Vector3.one;
	}

}

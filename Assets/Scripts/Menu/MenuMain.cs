﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMain : MonoBehaviour {

	public void Show(bool show) {
		gameObject.SetActive(show);
	}

	public void TriggerStart() {
		GameController.SendMessage(GameMessage.StartGame);
	}
}

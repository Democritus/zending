﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player {

	public int remainingPulses = 3;
	public PulsePattern defaultPattern;

	public Player() {
		defaultPattern = new PulsePattern();
		defaultPattern.maxRadius = 4f;
		defaultPattern.attackEase = Easing.EaseType.CircularOut;
		defaultPattern.attackTime = 4f;
		defaultPattern.sustainTime = 0f;
		defaultPattern.decayEase = Easing.EaseType.SinusoidalIn;
		defaultPattern.decayTime = 4f;
	}

	public void RefreshPulses() {
		remainingPulses = 3;
	}

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

	public Camera gameCam;
	public Player player;
	public Pulse pulsePrefab;
	public SpriteRenderer background;
	public HudControl hudControl;

	WorldGenerator worldGen;
	Dot[] currentDots;

	[Header("Menus")]
	public MenuSkipLevel skipLevelUI;
	public MenuMain mainMenu;

	ThemeBase currentTheme;
	int currentThemeIndex;
	public ThemeBase[] allThemes;

	// Current level
	public static bool isGameRunning { get; private set; }

	bool levelEnded = false;
	List<Pulse> playerPulses;
	bool hasActivePulse = false;
	public int numConversions = 0;
	public int targetConversions = 20;

	private static GameController instance;

	public static void SendMessage(GameMessage message) {
		instance.HandleGameMessage(message);
	}

	public static ThemeBase GetTheme() {
		if (instance == null)
			return null;

		return instance.currentTheme;
	}

	void Start() {
		instance = this;
		currentTheme = allThemes[currentThemeIndex];

		player = new Player();
		worldGen = GetComponent<WorldGenerator>();

		GUIFader.Fade(GUIFader.LODFadeMode.FadeIn, 2f);
		Pulse.onCheckPulse += HandleOnCheckPulse;

		DoMainMenu();
	}

	void UpdateBackground() {
		background.sprite = currentTheme.GetBackgroundSprite();
		background.color = currentTheme.GetBackgroundColor();
	}

	void HandleGameMessage(GameMessage message) {
		switch (message) {
			case GameMessage.StartGame:
				TriggerStartGame();
				break;

			case GameMessage.EndLevel:
				DoMainMenu();
				break;

			case GameMessage.NextLevel:
				DoNextLevel();
				break;

			case GameMessage.RestartLevel:
				DoRestartLevel();
				break;

			case GameMessage.MainMenu:
				DoMainMenu();
				break;

			default:
				break;
		}
	}

	void TriggerStartGame() {
		GUIFader.Fade(GUIFader.LODFadeMode.Bounce, 3f, DoStartGame);
	}

	void DoStartGame() {
		MusicManager.PlayMusic("mus_lv"+currentTheme.audioSet+"_loop");	

		hudControl.SetBlur(false);
		hudControl.ShowShots(true);
		mainMenu.Show(false);
		skipLevelUI.ShowRedo(false);
		skipLevelUI.ShowNext(false);

		// Recenter camera
		numConversions = 0;
		levelEnded = false;

		RemovePlayerPulses();
		player.RefreshPulses();
		hudControl.UpdateShots(player.remainingPulses);
		hudControl.SetProgressColor(currentTheme.GetProgressColor());
		hudControl.SetShotColors(currentTheme.GetShotColorA(), currentTheme.GetShotColorB());
		UpdateBackground();
		UpdateHUDProgress();

		worldGen.ClearDots(currentDots);
		currentDots = worldGen.SpawnDots();

		isGameRunning = true;
	}

	void DoMainMenu() {
		isGameRunning = false;

		skipLevelUI.ShowRedo(false);
		skipLevelUI.ShowNext(false);
		hudControl.SetBlur(true);
		hudControl.ShowShots(false);
		mainMenu.Show(true);

		MusicManager.PlayMusic("mus_intro_loop");	

		worldGen.ClearDots(currentDots);
		UpdateHUDProgress();
	}

	void Update () {
		if (!isGameRunning)
			return;

		if (Input.GetKeyDown(KeyCode.Space)) {
			numConversions = targetConversions;
			ShowButtonNext();
			ShowButtonRedo();
		}
		
		if (GetFireInput())
			TryToFirePlayerPulse(Input.mousePosition);

		if (!hasActivePulse && !levelEnded && (player.remainingPulses == 0)) {
			if (!HasEnoughDots())
				ShowButtonRedo();

			levelEnded = true;
		}
		else {
			hasActivePulse = false;
		}
	}

	// Fire Pulses

	Vector3 clickMousePos;
	float clickMouseTime;

	bool GetFireInput() {
		if (Input.GetMouseButtonDown(0)) {
			clickMousePos = Input.mousePosition;
			clickMouseTime = Time.time;
		}
		
		if (Input.GetMouseButtonUp(0)) {
			if (Vector2.Distance(clickMousePos, Input.mousePosition) >= 10f)
				return false;
			if (Time.time >= clickMouseTime + 0.25f)
				return false;

			return true;
		}

		return false;
	}

	void TryToFirePlayerPulse(Vector3 screenPos) {
		if (player.remainingPulses == 0)
			return;

		if (player.remainingPulses > 0)
			player.remainingPulses--;

		var worldPos = Camera.main.ScreenToWorldPoint(screenPos);
		worldPos.z = 0f;
		SpawnPlayerPulse(worldPos);
		hudControl.UpdateShots(player.remainingPulses);
	}

	void SpawnPlayerPulse(Vector3 worldPos) {
		var pulse = Instantiate(pulsePrefab, worldPos, Quaternion.identity).GetComponent<Pulse>();
		pulse.Init(null, player.defaultPattern);
		pulse.gameObject.SetActive(true);
		playerPulses.Add(pulse);
		SoundManager.PlaySfx("snd_player_initiate_1time");
	}

	void SpawnDotPulse(Dot dot) {
		var pattern = dot.GetPulsePattern();

		if (pattern.maxRadius == 0)
			return;
		
		var pulse = Instantiate(pulsePrefab, dot.transform) as Pulse;
		pulse.Init(dot, pattern);
		pulse.gameObject.SetActive(true);
	}

	void HandleOnCheckPulse(Pulse pulse, float radius) {
		hasActivePulse = true;
		var worldPos = pulse.transform.position;

		for (int i = 0; i < currentDots.Length; i++) {
			if (currentDots[i] == null || currentDots[i].IsConverted())
				continue;

			if (Vector3.Distance(worldPos, currentDots[i].transform.position) <= radius) {
				currentDots[i].ConvertDot(pulse);
				SpawnDotPulse(currentDots[i]);
				numConversions++;

				if (numConversions == targetConversions) 
					ShowButtonNext();
				
				UpdateHUDProgress();
			}
		}
	}

	void RemovePlayerPulses() {
		if (playerPulses != null) 
			foreach (var item in playerPulses) 
				if (item != null)
					Destroy(item.gameObject);

		playerPulses = new List<Pulse>();
	}

	// HUD

	void UpdateHUDProgress() {
		float percentage = currentDots != null ? numConversions / (float) targetConversions : 0f;
		hudControl.UpdateProgress(percentage);
	}

	// GAME CONTROL

	void ShowButtonNext() {
		SoundManager.PlaySfx("snd_won_1time");
		skipLevelUI.ShowNext(true);
	}

	void ShowButtonRedo() {
		SoundManager.PlaySfx("snd_wonnot_1time");
		skipLevelUI.ShowRedo(true);
	}

	bool HasEnoughDots() {
		return numConversions >= targetConversions;
	}

	void DoRestartLevel() {
		TriggerStartGame();
	}

	void DoNextLevel() {
		targetConversions += 10;

		SetTheme(currentThemeIndex + 1);
		TriggerStartGame();
	}

	void SetTheme(int index) {
		currentThemeIndex = index >= allThemes.Length ? 0 : index;
		currentTheme = allThemes[currentThemeIndex];
		Debug.Log("THeme Index " + currentThemeIndex);
	}

}

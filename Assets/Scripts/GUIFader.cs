﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIFader : MonoBehaviour {

	public enum LODFadeMode { FadeIn, FadeOut, Bounce }

	public static void SetBlack(bool black) {
		CreateInstance();
		instance.currentColor = new Color(0f, 0f, 0f, black ? 1f : 0f);
		instance.gameObject.SetActive(black);
	}

	public static void Fade(LODFadeMode fadeMode, float duration, System.Action onComplete = null) {
		CreateInstance();
		instance.StartFade(fadeMode, duration, onComplete);
	}

	static void CreateInstance() {
		if (instance == null) {
			instance = new GameObject().AddComponent<GUIFader>();
			instance.gameObject.name = "GUIFader";
			instance.gameObject.SetActive(false);
		}
	}

	static GUIFader instance;

	Rect screenRect;
	bool bounce;
	float startAlpha;
	float targetAlpha;
	Color currentColor;
	float time;
	float duration;
	bool hasCompleted;
	System.Action onComplete;

	void StartFade(LODFadeMode fadeMode, float duration, System.Action onComplete) {
		this.screenRect = new Rect(0, 0, Screen.width, Screen.height);

		this.time = 0f;
		this.duration = duration;

		this.bounce = fadeMode == LODFadeMode.Bounce;

		this.startAlpha = fadeMode == LODFadeMode.FadeIn ? 1f : 0f;
		this.targetAlpha = fadeMode == LODFadeMode.FadeIn ? 0f : 1f;
		this.currentColor = new Color(0f, 0f, 0f, startAlpha);

		this.onComplete = onComplete;
		this.hasCompleted = false;

		instance.gameObject.SetActive(true);
	}

	void OnGUI () {
		if (!hasCompleted) {
			time += Time.deltaTime;
			currentColor = new Color(0f, 0f, 0f, Mathf.Lerp(startAlpha, targetAlpha, time / duration));
		}
		else if (currentColor.a <= 0f) {
			return;
		}

		GUI.color = currentColor;
		GUI.DrawTexture(screenRect, Texture2D.whiteTexture);

		if (time >= duration) {
			if (bounce) {
				time = 0f;
				startAlpha = 1f;
				targetAlpha = 0f;
				bounce = false;
				FireEvent();
			}
			else if (!hasCompleted) {
				hasCompleted = true;
				FireEvent();
			}
		}
	}

	void FireEvent() {
		if (onComplete != null) {
			onComplete.Invoke();
			onComplete = null;
		}
	}

}

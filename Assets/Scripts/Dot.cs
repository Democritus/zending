﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dot : MonoBehaviour {

	public int generation;

	float dotSize = 1f;
	bool isConverted = false;
	SpriteRenderer sprite;
	MovePattern movePattern;
	Vector3 startPos;

	Vector3? pushImpulse;
	float pushMagnitude;

	public void Init(MovePattern movePattern) {
		sprite = GetComponent<SpriteRenderer>();
		sprite.color = GameController.GetTheme().GetDotColorStrange();

		var dotSprite = GameController.GetTheme().GetDotSprite();

		if (dotSprite != null)
			sprite.sprite = dotSprite;

		transform.localScale = Vector3.one * 0.5f;
		SetMovePattern(movePattern);
	}

	void SetMovePattern(MovePattern pattern) {
		movePattern = pattern;
		startPos = transform.position;
		enabled = pattern != null; // If no pattern, disable update method
	}

	public bool IsConverted() {
		return isConverted;
	}

	public void ConvertDot(Pulse sourcePulse) {
		if (isConverted)
			return;

		transform.localScale = Vector3.one;
		generation = sourcePulse.GetGeneration() + 1;
		isConverted = true;	
		sprite.color = GameController.GetTheme().GetDotColorConverted();

		pushImpulse = transform.position - sourcePulse.transform.position;
		pushImpulse = pushImpulse.Value.normalized;
		pushMagnitude = sourcePulse.GetCurrentPushForce() * 0.25f;

		enabled = true;
		PlaySound();
	}

	void PlaySound() {
		int gen = Mathf.Clamp(generation - 1, 0, GameController.GetTheme().maxPulseGeneration);
		SoundManager.PlaySfx("snd_" + GameController.GetTheme().audioSet + "dim" + gen + "_1time");
	}

	public PulsePattern GetPulsePattern() {
		var radiusPattern = new PulsePattern();
		radiusPattern.maxRadius = dotSize * 1.5f;
		radiusPattern.attackEase = Easing.EaseType.SinusoidalOut;
		radiusPattern.attackTime = 1f;
		radiusPattern.sustainTime = 2f;
		radiusPattern.decayEase = Easing.EaseType.QuadraticInOut;
		radiusPattern.decayTime = 6f;
		return radiusPattern;
	}

	void Update() {
		if (pushImpulse != null) {   
			startPos += pushImpulse.Value * pushMagnitude * Time.deltaTime;
			pushMagnitude -= 0.2f * Time.deltaTime;

			if (pushMagnitude <= 0f) {
				pushImpulse = null;

				if (movePattern == null)
					enabled = false;
			}
		}
			
		if (movePattern != null)
			transform.position = startPos + movePattern.GetMoveOffset(Time.deltaTime);
		else if (pushImpulse != null)
			transform.position = startPos;
	}



}

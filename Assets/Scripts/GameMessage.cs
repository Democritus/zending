﻿public enum GameMessage {
	StartGame,
	EndLevel,
	RestartLevel,
	NextLevel,
	MainMenu
}

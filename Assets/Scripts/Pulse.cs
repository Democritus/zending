﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pulse : MonoBehaviour {

	public static event System.Action<Pulse, float> onCheckPulse;

	const int spriteSize = 512;
	const int pixelsPerWorldUnit = 100;

	[SerializeField] SpriteRenderer sprite;

	Dot owner;
	PulsePattern radiusPattern;
	float radiusMultiplier = 0f;
	float lifeTime = 0f;

	// NOTE: Pulse Sprite needs to have PixelsPerUnit equal to texture Size to be 1x1 unit in size
	public void Init(Dot owner, PulsePattern pattern) {
		this.owner = owner;
		radiusPattern = pattern;
		transform.localScale = Vector3.zero;

		sprite.color = GameController.GetTheme().GetPulseColor();
		var pulseSprite = GameController.GetTheme().GetPulseSprite();

		if (pulseSprite != null) 
			sprite.sprite = pulseSprite;

		if (owner != null) {
			var pulseParticle = GameController.GetTheme().GetPulseParticle();

			if (pulseParticle != null)
				Instantiate(pulseParticle, transform);
		}
	}

	public float GetCurrentRadius() {
		return radiusPattern.maxRadius * radiusMultiplier;
	}

	void Update() {
		lifeTime += Time.deltaTime;
		radiusMultiplier = GetRadiusMultiplier();

		if (radiusMultiplier <= 0f) {
			Destroy(gameObject); // Pool instead;
			return;
		}
			
		transform.localScale = Vector3.one * 2f * GetCurrentRadius();

		if (onCheckPulse != null)
			onCheckPulse(this, GetCurrentRadius());
	}

	float GetRadiusMultiplier() {
		if (lifeTime < radiusPattern.attackTime) {
			return Easing.Ease(radiusPattern.attackEase, lifeTime / radiusPattern.attackTime);
		}
		else if (lifeTime < radiusPattern.attackTime + radiusPattern.sustainTime) {
			return 1f;
		}
		else if (lifeTime < radiusPattern.attackTime + radiusPattern.sustainTime + radiusPattern.decayTime) {
			return 1f - Easing.Ease(radiusPattern.decayEase, (lifeTime - radiusPattern.sustainTime - radiusPattern.attackTime) / radiusPattern.decayTime);
		}
		return 0f;
	}

	public float GetCurrentPushForce() {
		return (1f - lifeTime / (radiusPattern.attackTime + radiusPattern.sustainTime + radiusPattern.decayTime)) * radiusPattern.maxRadius;
	}

	public int GetGeneration() {
		return owner != null ? owner.generation : 0;
	}
}

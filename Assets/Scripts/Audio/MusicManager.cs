using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : AudioManager {

    // Singleton
    private static MusicManager instance = null;
    public static MusicManager Instance { get { return instance; } }

	[SerializeField] string resourcesPath = "Sounds/";
    [SerializeField] float fadeInTime = 2f;
    [SerializeField] float fadeOutTime = 2f;

	protected AudioSource audioSourceMusic;

	private string currentTrack = "";
	private string lastTrack = "";
	private string nextTrack = "";

    // Initalization
    void Awake() {
        if (Instance != null) {
            Destroy(gameObject);
            return;
        }

		instance = this;
        audioSourceMusic = gameObject.AddComponent<AudioSource>();
		audioSourceMusic.loop = true;
		audioSourceMusic.playOnAwake = false;
    }

    // Delivery of Audio Clips & Sources

    // Public Playback Methods

    public static void PlayMusic(string trackName) {
		if (Instance == null)
			return;

		Instance.StartTrack(trackName);
	}

	public static void StopMusic() {
		if (Instance == null)
			return;

		Instance.StopTrack();
	}

	public static bool IsCurrentlyPlaying() {
		if (Instance == null)
			return false;

		return Instance.currentTrack != "";
	}

    // internal handling methods for playback

    private void StartTrack(string trackName) {
		if (!isOn || (audioSourceMusic.isPlaying && trackName == currentTrack) || GetAudioClip(resourcesPath + trackName) == null)
			return;

		StopAllCoroutines();

		if (audioSourceMusic.isPlaying) {
			nextTrack = trackName;
			FadeOutTrack(currentTrack);
		} 
		else {
			FadeInTrack(trackName);
		}
	}

    private void StopTrack() {
        if (currentTrack == "")
            return;
        
		StopAllCoroutines();
		FadeOutTrack(currentTrack);
	}

    // Do Fade Ins

    private void FadeInTrack(string trackName, float duration = -1f) {
		var music = GetAudioClip(resourcesPath + trackName);

		if (music == null)
			return;

		lastTrack = currentTrack;
		currentTrack = trackName;
		StartCoroutine(DoFadeInSpeaker(trackName, music, duration));
	}

	private IEnumerator DoFadeInSpeaker(string trackName, AudioClip music, float duration) {
		float time = 0f;

		audioSourceMusic.clip = music;
		audioSourceMusic.volume = 0f;
		audioSourceMusic.Play();

		if (duration < 0f)
			duration = fadeInTime; 

		while (time < duration) {
			yield return null;
			time += Time.deltaTime;
			audioSourceMusic.volume = Mathf.Lerp(0f, 1f, time/duration);	
		}

		audioSourceMusic.volume = 1f;
	}

    // Do Fade Outs

	private void FadeOutTrack(string trackName, float duration = -1f) {
		if (!audioSourceMusic.isPlaying)
			return;
		
		lastTrack = currentTrack;
		StartCoroutine(DoFadeOutSpeaker(duration));
	}

	private IEnumerator DoFadeOutSpeaker(float duration) {
		float time = 0f;
		float startValue = audioSourceMusic.volume;

		if (duration < 0f)
			duration = fadeOutTime; 

		while (time < duration) {
			yield return null;
			time += Time.deltaTime;
			audioSourceMusic.volume = Mathf.Lerp(startValue, 0f, time / duration);
		}

		audioSourceMusic.volume = 0f;
		audioSourceMusic.Stop();

		if (nextTrack != "") {
			FadeInTrack(nextTrack);
			nextTrack = "";
		}
	}

    // LIfecycle

    protected override void SetEnabled(bool turnOn) {
        if (this.isOn == turnOn)
            return;

        base.SetEnabled(turnOn);

        if (!isOn && currentTrack != "") {
            StopAllCoroutines();
            lastTrack = currentTrack;
            TurnOffAllAudio();
        }
        else if (isOn && lastTrack != "") { 
             StartTrack(lastTrack);
        }
    }

    public static void Reset() {
        if (Instance == null)
            return;

        Instance.TurnOffAllAudio();
    }

    private void TurnOffAllAudio() {
        audioSourceMusic.Stop();
        currentTrack = "";
    }

    private void OnDestroy() {
        TurnOffAllAudio();

        if (Instance == this)
            instance = null;
    }
}

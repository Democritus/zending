﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : AudioManager {

    // Singleton
    private static SoundManager instance = null;
    public static SoundManager Instance { get { return instance; } }

    private List<AudioSource> audioSourcePool = new List<AudioSource>();

	// Fade Settings
	[SerializeField] string resourcesPath = "Sounds/";

    // Initalization

    void Awake() {
        if (Instance != null) 
            Destroy(gameObject);
        else
        	instance = this;
    }

    // Delivery of Audio Clips & Sources

    private int GetFreeAudioSourceId(AudioClip clip) {
        string name = clip.name;

        for (int i = 0; i < audioSourcePool.Count; i++) {
            if (audioSourcePool[i].isPlaying == false)
                return i;
        }

        return AddNewAudioSource(clip);
    }

    private int AddNewAudioSource(AudioClip clip) {
        int index = audioSourcePool.Count;
        AudioSource newSource = gameObject.AddComponent<AudioSource>();
        newSource.clip = clip;
        audioSourcePool.Add(newSource);
        return index;
    }

    private AudioSource GetAudioSource(int id) {
        if (id < audioSourcePool.Count) 
            return audioSourcePool[id];

        return null;
    }

    // Public Playback Methods

	public static AudioSource PlaySfx(string name, float volume = -1f, float pitch = -1f, bool loop = false) {
        if (Instance == null)
            return null;

		return Instance.PlaySound(name, volume, pitch, loop);
    }

    // internal handling methods for playback

	private AudioSource PlaySound(string name, float volume, float pitch, bool loop) {
        if (!isOn)
			return null;

		AudioClip sound = GetAudioClip(resourcesPath + name);

        if (sound == null)
            return null;

        int id = GetFreeAudioSourceId(sound);
        AudioSource source = GetAudioSource(id);

        if (source == null)
			return null;

		source.clip = sound;
        source.volume = (volume >= 0 ? volume : 1f);
		source.pitch = (volume >= 0 ? volume : 1f);
		source.loop = loop;
        source.Play();

        return source;
	}

    // LIfecycle

    protected override void SetEnabled(bool turnOn) {
        if (this.isOn == turnOn)
            return;

        base.SetEnabled(turnOn);

        if (!turnOn)
            TurnOffAllAudio();
    }

    private void TurnOffAllAudio() {
        foreach (AudioSource audioSource in audioSourcePool) 
            audioSource.Stop();
    }

    private void OnDestroy() {        
        TurnOffAllAudio();

        if (Instance == this)
            instance = null;
    }
}